package app

import (
	"fmt"

	"github.com/robertkrimen/otto"
)

func (p *Parameter) AssignReadsParameter(param *Parameter) {
	p.Value = param.Value
	p.Host = param.Host
	p.Clock = param.Clock
}

func newDefParameter() Parameter {
	return Parameter{
		Key:           "test.metric",
		Alias:         "Новый",
		Trigger:       "...",
		TriggerStatus: -1,
		Name:          "metric",
		Message:       "OK",
		ScriptError:   "Ошибка выполнения метрики",
		Novisible:     false,
		Host:          "localhost",
		Active:        false,
		Pid:           -1,
		ServiceIndex:  -1,
		ServerIndex:   -1,
	}
}

func (p *Parameter) CheckTrigger(vm *otto.Otto) error {
	if p.Trigger != nil {
		vm.Set(fmt.Sprint(p.Name), p.Value)
		_, err := vm.Eval(p.Trigger)
		if err != nil {
			p.ScriptError = err.Error()
			return fmt.Errorf("Ошибка выполнения триггера:", p.ScriptError, "code: ", p.Trigger)
		} else {
			p.Message, err = vm.Get("message")
			if err != nil {
				fmt.Println("Ошибка получения сообщения", err.Error())
			}
			status, err := vm.Get("trigger_status")

			// Init status before calculate -- json error reject
			p.TriggerStatus = -1
			if err != nil {
				fmt.Println("Ошибка получения статуса триггера", err.Error())
			}
			intStatus, _ := status.ToInteger()
			p.TriggerStatus = int(intStatus)
		}
	}
	return nil
}

func (p *Parameter) PublicName() string {
	return p.Alias
}

func (p *Parameter) Index() interface{} {
	return p.Pid
}

func (p *Parameter) Parent() int {
	return p.ServiceIndex
}
func (p *Parameter) Root() int {
	return p.ServerIndex
}

func (p *Parameter) Error() bool {
	return false
}

func (p *Parameter) Status() string {
	return p.Message.(string)
}
