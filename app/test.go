package app

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func (t *Test) HaveError() bool {
	var counter int32 = 0
	for _, p := range t.TestParts {
		if p.Result {
			counter++
		}
	}
	return counter != 0
}

func newDefTest() Test {
	return Test{Name: "Новый тест", Url: "localhost:8808", QueryType: "POST", index: -1}
}

func (t *Test) Run() error {
	fmt.Println("Fire test Run..")

	caCert, err := ioutil.ReadFile("/home/zipper/CertPostman/eduard.crt")
	if err != nil {
		log.Fatal(err)
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs:      caCertPool,
			},
		},
	}

	url := "https://evobox.studio-evolution.ru:443/api/auth/login"
	// url := "https://evobox.studio-evolution.ru:443/api/auth/login?serial=R2665200&hwid=3B0C-44DC-6B05-F66F"

	method := "POST"

	jsonData := map[string]string{"serial": "R2665200", "hwid": "3B0C-44DC-6B05-F66F"}
	jsonValue, _ := json.Marshal(jsonData)
	request, err := http.NewRequest(method, url, bytes.NewBuffer(jsonValue))

	// request, err := http.NewRequest(method, url, nil)

	request.Header.Set("Content-Type", "application/json")
	//request.Header.Add("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1dWlkIjoiZmU4NmVkMGEtNmM0OC0xMWVhLWIxZGEtOTAxYjBlZmUyZmMzIiwic3ViIjoyMjk2NiwiaXNzIjoiaHR0cHM6Ly9ldm9ib3guc3R1ZGlvLWV2b2x1dGlvbi5ydS9hcGkvYXV0aC9sb2dpbiIsImlhdCI6MTU4NDg4NzEzNSwiZXhwIjoxNTg0OTczNTM1LCJuYmYiOjE1ODQ4ODcxMzUsImp0aSI6IkVnb1JTRUlrTHpZeWVjS2MifQ.k8WcVY89cQhTjFN9M4LMk-hsNxT-wFkmVtgAQl9GqOU")

	// client := &http.Client {}
	// req, err := http.NewRequest(method, url, nil)
	fmt.Println("Request: ", request)

	if err != nil {
		fmt.Println(err)
	}

	res, err := client.Do(request)
	if err == nil {
		defer res.Body.Close()
		body, errb := ioutil.ReadAll(res.Body)
		if errb != nil {
			fmt.Println("Error do ReadAll: ", errb.Error())
			return errb
		}

		fmt.Println(string(body))
		t.Result = fmt.Sprintf("%s", body)
		return nil
	}
	fmt.Println("Error do request: ", err.Error())
	return err
}

func (t *Test) deletePart(index int) error {
	if index < 0 {
		return fmt.Errorf("Index low than range ", index)
	}
	CollectoMutex.Lock()
	t.TestParts[len(t.TestParts)-1], t.TestParts[index] = t.TestParts[index], t.TestParts[len(t.TestParts)-1]
	t.TestParts = t.TestParts[:len(t.TestParts)-1]
	CollectoMutex.Unlock()
	return nil
}

func (t *Test) PublicName() string {
	return t.Name
}

func (t *Test) Index()  interface{} {
	return  t.index
}

func (t *Test) Parent() int {
	return -1
}
func (t *Test) Root() int  {
	return -1
}

func (t *Test) Error() bool {
	return false
}

func (t *Test) Status() string {
	return "Успешно"
}