package app

import (
	"fmt"
	"io"
	"net"

	"github.com/robertkrimen/otto"
)

// ReadParameters чтение параметров сервера. Соединение должно быть живым.
func (s *Server) ReadParameters() error {
	vm := otto.New()

	for j, g := range s.Services {
		for i, _ := range g.Parameters {

			p := &s.Services[j].Parameters[i]

			if p.Active == true {
				continue
			}

			// s.Error = "Соединение установлено"
			conn, err := net.Dial("tcp", s.Ipaddr)

			if conn == nil || err != nil {
				fmt.Println("Error connection: ", err.Error());
				continue
			}

			query := []byte(p.Key)

			// Fill buffer
			buffer := append([]byte("ZBXD\x01"), PackDataLen(query)...)
			buffer = append(buffer, query...)

			// Sent packet to zabbix
			n, err := conn.Write(buffer)
			if n == 0 || err != nil {
				fmt.Println("Ошибка отправки:", err)
				conn.Close()
				continue
			}

			// fmt.Println("Read the reply.")
			buff := make([]byte, 0, 4096) // big buffer
			tmp := make([]byte, 256)      // using small tmo buffer for demonstrating

			var reads int = 0
			for {
				m, err := conn.Read(tmp)
				if err != nil {
					if err != io.EOF {
						fmt.Println("Error reading: ", err)
					}
					// fmt.Println("Error reading: ", err)
					break
				}
				buff = append(buff, tmp[:m]...)
				reads += m
			}

			// fmt.Println("Reads: ", string(buff))

			if len(buff) > 13 {
				// Assign reads value from incoming buff
				p.Value = string(buff[13:])

				// Assign system host name for ident of incoming paarameters
				s.AssignSystemHostName(&(s.Services[j].Parameters[i]))
				p.CheckTrigger(vm)

			}
			conn.Close()
		}
		// fmt.Printf("Отлично, данные сервера %s: %v\n", s.Alias, s)

	}

	return nil
}

func (s *Server) StatusTypeCount(status int) int {
	counter := 0
	for _, g := range s.Services {
		counter += g.StatusTypeCount(status)
	}
	return counter
}

func (s *Server) HaveToggled(group *Service) bool {
	var counter int32 = 0
	if group == nil {
		if s.Error() == true {
			return true
		}
		for _, g := range s.Services {
			counter += g.Toggled()
		}
	} else {
		counter += group.Toggled()
	}
	return counter != 0
}

func (s *Server) ArrangeByTriggers(toggled bool) interface{} {
	data := make(map[string][]Parameter)
	for _, g := range s.Services {
		var parameters []Parameter
		for _, p := range g.Parameters {
			if p.Trigger != nil {
				if toggled == true {
					// fmt.Println("TriggerStatus is a ", status)
					if p.TriggerStatus >= 1 {
						// Is toggled trigger - working
						parameters = append(parameters, p)
					}
				} else {
					// Append all triggers for configure
					parameters = append(parameters, p)
				}
			}
		}
		if len(parameters) != 0 {
			data[g.Name] = parameters
		}
	}
	return data
}

// Hostname Got a hostname from Agent group of metrics
func (s *Server) AgentHostname() interface{} {
	for _, g := range s.Services {
		for _, p := range g.Parameters {
			if p.Key == "agent.hostname" {
				return p.Value
			}
		}
	}

	return nil
}

func (s *Server) AssignSystemHostName(p *Parameter) error {
	if p.Key == "system.hostname" {
		s.Host = fmt.Sprintf("%v", p.Value)
		return nil
	}
	return fmt.Errorf("Parameter is not a system.hostanme")
}

func (s *Server) deleteService(index int) error {
	if index < 0 {
		return fmt.Errorf("Index low than range ", index)
	}
	CollectoMutex.Lock()
	s.Services[len(s.Services)-1], s.Services[index] = s.Services[index], s.Services[len(s.Services)-1]
	s.Services = s.Services[:len(s.Services)-1]
	CollectoMutex.Unlock()
	return nil
}

func (s *Server) PublicName() string {
	return s.Alias
}

func (s *Server) Index() interface{} {
	return s.index
}

func (s *Server) Parent() int {
	return -1
}

func (s *Server) Root() int  {
	return -1
}

func (s *Server) Error() bool {
	return s.error
}

func (s *Server) Status() string {
	return s.status
}