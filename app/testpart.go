package app

func (p *TestPart) PublicName() string {
	return p.Name
}

func (p *TestPart) Index()  interface{} {
	return  p.index
}

func (p *TestPart) Parent() int {
	return p.TestIndex
}
func (p *TestPart) Root() int  {
	return -1
}

func (p *TestPart) Error() bool {
	return p.Result
}

func (p *TestPart) Status() string {
	return p.status
}
