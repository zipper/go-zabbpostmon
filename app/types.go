package app

import (
	"encoding/binary"
)

type Base interface {
	// Got a public Name of object for form displaying
	PublicName() string

	// Got index of object
	Index() interface{}

	// Got parent index
	Parent() int

	// Got first root parent index
	Root() int

	// Got error state
	Error() bool

	// Got status string
	Status() string
}

// User UI config struct
type UiConfig struct {
	Page        uint   `json:"page"`         // Current server page layout
	PageCaption string `json:"page_caption"` // Caption of current server page
}

// Parameter - the struct, contained key of value and alias for parametr,
// quering from Zabbix agent in passive mode and showing in HTML table or
// incoming data of struct from the agent, turned active mode on
type Parameter struct {
	Key           string      `json:"key"`            // Key of requested value
	Alias         string      `json:"alias"`          // Alias for header
	Value         interface{} `json:"value"`          // Value of key
	Trigger       interface{} `json:"trigger"`        // Trigger code source
	TriggerStatus int         `json:"trigger_status"` // Trigger status -- 0 - info, 1 - warning, 2 - attention, 3 - fail
	Name          interface{} `json:"name"`           // Name fof JS interpretator value
	Message       interface{} `json:"message"`        // Message after check trigger
	ScriptError   interface{} `json:"script_error"`   // Error string after execute script
	Novisible     bool        `json:"novisible"`      // Flag of visibility of Parametr for showing
	Host          string      `json:"host"`           // Host name received from agent -- idents of receiving params!!
	Clock         uint32      `json:"clock"`          // The current timestamp of parameter counting
	Active        bool        `json:"active`          // Type of group - Active or Passive, direction of quering parameters
	Pid           int         `json:"index"`          // Index in array
	ServiceIndex  int         `json:"service_index"`  // Index of parent service
	ServerIndex   int         `json:"server_index"`   // Index of parent service server
}

// Group is a struct, container metrics for thematic template
type Service struct {
	Parameters  []Parameter `json:"parameters"`   // Parameters arrays with diferent source - active/passive
	Name        string      `json:"name"`         // Name of group
	index       int         `json:"index"`        // Index in the parent Services array
	serverIndex int         `json:"server_index"` // Index of parent server
	// Condition   int         `json:"condition"`    // Condition of job ability in percents
}

// Server struct which contains
// queried parameters list
type Server struct {
	Ipaddr   string    `json:"ipaddr"`  // IP address? included port aka "0.0.0.0:00"
	Alias    string    `json:"alias"`   // Alias of server for display
	Port     int       `json:"port"`    // Integer valued port
	Host     string    `json:"host"`    // Host name received from agent -- idents of receiving params!!
	Services []Service `json:"groups"`  // Groups array
	Cluster  int       `json:"cluster"` // Cluster Integer index identifier
	Ui       UiConfig  `json:"ui"`      // The object for ui config defines
	error    bool      `json:"error"`   // State if server connection failed
	status   string    `json:"status"`  // String message about error of success status
	index    int       `json:"index"`   // Index in array - for WEB application
	// Cluster  string    `json:"cluster"` // Cluster String identifier
}

// TestPart description struct
type TestPart struct {
	Name      string `json:"name"`       // Name of test partial
	Script    string `json:"script"`     // Testing script
	Result    bool   `json:"result"`     // Result of running
	TestIndex int    `json:"test_index"` // Index of parent test in array tests
	status    string `json:"error"`      // Status message
	index     int    `json:"index"`      // Index in a parent array
}

// Test strcut of test API data fields with cost scripts partials
type Test struct {
	Name        string     `json:"name"`         // Test group name
	TestParts   []TestPart `json:"test_parts"`   // Array of tests in test group
	Url         string     `json:"url"`          // API Url test
	QueryType   string     `json:"query_type"`   // REST query type
	Result      string     `json:"result"`       // Body of responce
	index       int        `json:"index"`        // index test in array
}

// Config struct which contains
// an array of servers
type AppConfig struct {
	HttpPort       int                    `json:"http_port"`        // IP port of WEB service evomon
	ActivePort     int                    `json:"active_port"`      // IP port for incoming metrics aka zabbix-server
	Tickdelay      int                    `json:"tickdelay"`        // Delay of query servers parameters in Msec
	Timeout        int64                  `json:"timeout"`          // Timeout dialing of agent Msec
	Servers        []Server               `json:"servers"`          // Array of servers
	Tests          []Test                 `json:"tests"`            // Array of Tests settings
	EnvData        map[string]interface{} `json:"env_data"`         // Environment for tests
	Clusters       []string               `json:"clusters"`         // Clusters name array
	UiTestExpanded bool                   `json:"ui_test_expanded"` // Open tests tree flag
	UiStateInc     int                    `json:"ui_state_inc"`     // Incrementable number of state label
	UiClusterId    int                    `json:"ui_cluster_id"`    // Current cluster for select filter
	// Serial         string   `json:"serial"`           // Device serial number
	// Hwid           string   `json:"hwid"`             // Hardware device id
	// ApiHost        string   `json:"api_host"`         // API host address for tests
	// Token          string   `json:"token"`            // Save token for request field
}

// Reads - struct contained data, received from agent in active mode
type Reads struct {
	Request string      `json:"request"`
	Data    []Parameter `json:"data"`
}

type Env struct {
	Key   string
	Value interface{}
}

// Const querytypes array
var QueryTypes = map[int]string{
	0: "GET",
	1 : "POST",
}

// Count the bytes data len end pack it value to littleendian
// 8-bytes array
func PackDataLen(data []byte) []byte {
	dataLen := make([]byte, 8)
	binary.LittleEndian.PutUint32(dataLen, uint32(len(data)))
	return dataLen
}

// Unpack the bytes data len акщь littleendian 8-bytes array
// to integer
func UnpackDataLen(data []byte) uint32 {
	return binary.LittleEndian.Uint32(data)
}
