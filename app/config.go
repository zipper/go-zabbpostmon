package app

import (
	// "bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/url"
	"os"
	"strconv"
	"strings"
	// "strings"
	"sync"
	"time"

	"github.com/robertkrimen/otto"
)

// GLOBALS
// App root dir
var RootDir string

// Config file full pathname
var ConfigFile string

// Collector muutex - blocked thread for deleting and creating server from collating process
var CollectoMutex = &sync.Mutex{}

func (c *AppConfig) Init() {
	c.UiClusterId = -1
}

func checkBoxState(state string) bool {
	return state == "true" || state == "on"
}

func (c *AppConfig) Load(fileName string) error {
	// Open our jsonFile
	jsonFile, err := os.Open(fileName)
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
		return err
	}
	// fmt.Println("Successfully Opened ", configFile)

	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	// read our opened jsonFile as a byte array.
	byteValue, _ := ioutil.ReadAll(jsonFile)

	// we unmarshal our byteArray which contains our
	// jsonFile's content into 'servers' which we defined above
	json.Unmarshal(byteValue, c)

	// Set index of server object for WEB controls
	for i, s := range c.Servers {
		s.index = i
	}

	return nil
}

func (c *AppConfig) Save(fileName string) error {
	file, err := json.MarshalIndent(c, "", "    ")
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(fileName, file, 0644)
	return err
}

func (c *AppConfig) AssignIncomingValue(param *Parameter, vm *otto.Otto) error {
	for i, s := range c.Servers {
		if s.Host == param.Host {
			for j, g := range s.Services {
				for k, _ := range g.Parameters {
					p := &c.Servers[i].Services[j].Parameters[k]
					if param.Key == p.Key && p.Active == true {
						p.AssignReadsParameter(param)
						return p.CheckTrigger(vm)
					}
				}
			}
		}
	}
	return fmt.Errorf("Parameter is not accessible..")
}

func (c *AppConfig) AssignActiveReads(entries *Reads) error {
	vm := otto.New()
	// Save received data to equal groups
	for _, param := range entries.Data {
		c.AssignIncomingValue(&param, vm)
	}
	return nil
}

func (c *AppConfig) Collate() {
	CollectoMutex.Lock()
	for j, s := range c.Servers {
		d := net.Dialer{Timeout: time.Duration(c.Timeout)}

		conn, err := d.Dial("tcp", s.Ipaddr)
		if err != nil {
			c.Servers[j].error = true
			c.Servers[j].status = err.Error()
			continue
		}

		conn.Close()
		c.Servers[j].error = false
		c.Servers[j].status = "Данные получены"
		c.Servers[j].ReadParameters()
	}
	CollectoMutex.Unlock()
}

func (c *AppConfig) deleteServer(index int) error {
	if index < 0 {
		return fmt.Errorf("Index low than range ", index)
	}
	CollectoMutex.Lock()
	c.Servers[len(c.Servers)-1], c.Servers[index] = c.Servers[index], c.Servers[len(c.Servers)-1]
	c.Servers = c.Servers[:len(c.Servers)-1]
	CollectoMutex.Unlock()
	return nil
}

func (c *AppConfig) deleteTest(index int) error {
	if index < 0 {
		return fmt.Errorf("Index low than range ", index)
	}
	CollectoMutex.Lock()
	c.Tests[len(c.Tests)-1], c.Tests[index] = c.Tests[index], c.Tests[len(c.Tests)-1]
	c.Tests = c.Tests[:len(c.Tests)-1]
	CollectoMutex.Unlock()
	return nil
}

func (c *AppConfig) CollateAllTriggersByGroups(toggled bool, order bool) interface{} {
	data := make(map[string]map[string][]Parameter)

	for _, s := range c.Servers {
		for _, g := range s.Services {
			if !s.HaveToggled(&g) {
				continue
			}
			var pArray []Parameter
			for _, p := range g.Parameters {
				if toggled == true {
					if p.TriggerStatus > 1 {
						pArray = append(pArray, p)
					}
				} else {
					pArray = append(pArray, p)
				}
			}

			host := s.Host
			if host == "" {
				host = s.Alias
			}

			if order == true {
				// Is a server-groups order
				if data[host] == nil {
					data[host] = make(map[string][]Parameter)
				}
				data[host][g.Name] = pArray
			} else {
				// Is a server-groups order
				if data[g.Name] == nil {
					data[g.Name] = make(map[string][]Parameter)
				}
				data[g.Name][host] = pArray
			}
			// log.Println("data = ", data, ", array = ", pArray)

		}
	}
	return data
}

func (c *AppConfig) StartIncrementer() int {
	c.UiStateInc = 0
	return c.UiStateInc
}

func (c *AppConfig) NextIncrement() int {
	c.UiStateInc = c.UiStateInc + 1
	return c.UiStateInc
}

func (c *AppConfig) Increment() int {
	return c.UiStateInc
}

// TODO: refactor this function as universal for all objects by interface{} representation
// For textarea displaying body
func (c *AppConfig) testBody(t int) string {
	body, _ := json.MarshalIndent(c.Tests[t], "                    ", "      ")
	rawin := json.RawMessage(body)
	rawbytes, _ := rawin.MarshalJSON()
	r := strings.NewReplacer("\\r\\n", "\r\n",
		"\\u003c", "<",
		"\\u003e", ">")
	return r.Replace(fmt.Sprintf("%s", rawbytes))
}

func (c *AppConfig) prepareTestToForm(values url.Values, test *Test, ref string) (string, string, error) {
	var body string = ""
	t, errt := strconv.Atoi(values.Get("t"))
	if errt == nil {
		c.Tests[t].index = t
		body := c.testBody(t)
		r, errt := strconv.Atoi(values.Get("r"))
		if errt == nil && r == 1 {
			// Run test and collate result
			c.Tests[t].Run()
		}
		// Store JSON representation of service object
		*test = c.Tests[t]
		return body, "/test?t=" + values.Get("t"), nil
	}
	return body, ref, errt
}

func (c *AppConfig) submitFormTestUpdate(values url.Values, ref string) (string, error) {
	var err error = errors.New("submitFormTestUpdate: Ошибка чтения формы")

	backRef := ref

	fmt.Println("values: ", values)

	t, errt := strconv.Atoi(values.Get("Index"))

	fmt.Println("t: ", t)

	if errt == nil  {
		backRef = "/test?t=" + values.Get("Index")

		var defTest = newDefTest()

		if t == -1 {
			c.Tests = append(c.Tests, defTest)
			t = len(c.Tests) - 1
			backRef = "/test?t=" + values.Get("Index")
		}

		c.Tests[t].Name = values.Get("Name")
		c.Tests[t].Url = values.Get("Url")
		q, errq := strconv.Atoi(values.Get("QueryType"))
		if errq == nil {
			c.Tests[t].QueryType = QueryTypes[q]
		}
		c.Tests[t].index = t

		return backRef, c.Save(ConfigFile)
	}

	return backRef, err
}

func (c *AppConfig) submitFormTestDelete(values url.Values) error {
	var err error = errors.New("submitFormServiceDelete: Ошибка чтения формы")
	// s, errs := strconv.Atoi(values.Get("Parent"))
	// v, errv := strconv.Atoi(values.Get("Index"))
	// if errs == nil && errv == nil {
	// 	err = c.Servers[s].deleteService(v)
	// 	if err == nil {
	// 		return c.Save(ConfigFile)
	// 	}
	// }
	return err
}

func (c *AppConfig) prepareParamToForm(values url.Values, parameter *Parameter, ref string) (string, error) {
	s, errs := strconv.Atoi(values.Get("s"))
	if errs == nil {
		parameter.ServerIndex = s
		v, errv := strconv.Atoi(values.Get("v"))
		if errv == nil {
			parameter.ServiceIndex = v
			p, errp := strconv.Atoi(values.Get("p"))
			if errp == nil {
				c.Servers[s].Services[v].Parameters[p].ServerIndex = s
				c.Servers[s].Services[v].Parameters[p].ServiceIndex = v
				c.Servers[s].Services[v].Parameters[p].Pid = p
				*parameter = c.Servers[s].Services[v].Parameters[p]
				return "/group?s=" + values.Get("s") +
					"&v=" + values.Get("v"), nil
			}
		}
	} else {
		return "/group?s=" + values.Get("s") +
			"&v=" + values.Get("v"), errs
	}
	return ref, nil
}

func (c *AppConfig) submitFormParamUpdate(values url.Values, ref string) (string, error) {
	var err error = errors.New("submitFormParamUpdate: Ошибка чтения формы")

	fmt.Println("submitFormParamUpdate ", values)

	backRef := ref

	s, errs := strconv.Atoi(values.Get("ServerIndex"))
	v, errv := strconv.Atoi(values.Get("ServiceIndex"))
	p, errp := strconv.Atoi(values.Get("Index"))

	if errs == nil && errv == nil && errp == nil {
		backRef = "/group?s=" + values.Get("ServerIndex") + "&v=" + values.Get("ServiceIndex")

		if p == -1 {
			c.Servers[s].Services[v].Parameters = append(c.Servers[s].Services[v].Parameters, newDefParameter())
			p = len(c.Servers[s].Services[v].Parameters) - 1
		}

		c.Servers[s].Services[v].Parameters[p].Key = values.Get("Key")
		c.Servers[s].Services[v].Parameters[p].Alias = values.Get("Alias")
		c.Servers[s].Services[v].Parameters[p].Trigger = values.Get("Trigger")
		c.Servers[s].Services[v].Parameters[p].Name = values.Get("Name")
		c.Servers[s].Services[v].Parameters[p].Message = values.Get("Message")
		c.Servers[s].Services[v].Parameters[p].ScriptError = values.Get("ScriptError")
		c.Servers[s].Services[v].Parameters[p].Novisible, _ = strconv.ParseBool(values.Get("Novisible"))
		c.Servers[s].Services[v].Parameters[p].Host = values.Get("Host")
		c.Servers[s].Services[v].Parameters[p].Active, _ = strconv.ParseBool(values.Get("Active"))
		c.Servers[s].Services[v].Parameters[p].ServerIndex = s
		c.Servers[s].Services[v].Parameters[p].ServiceIndex = v
		c.Servers[s].Services[v].Parameters[p].Pid = p

		return backRef, c.Save(ConfigFile)
	}

	return backRef, err
}

func (c *AppConfig) submitFormParamDelete(values url.Values) error {
	var err error = errors.New("submitFormParamDelete: Ошибка чтения формы")
	s, errs := strconv.Atoi(values.Get("Root"))
	v, errv := strconv.Atoi(values.Get("Parent"))
	p, errp := strconv.Atoi(values.Get("Index"))
	if errs == nil && errv == nil && errp == nil {
		err = c.Servers[s].Services[v].deleteParameter(p)
		if err == nil {
			return c.Save(ConfigFile)
		}
	}
	return err
}

func (c *AppConfig) prepareEnvToForm(values url.Values, env *Env, ref string) (string, string, error) {
	var body string = ""
	e := values.Get("e")
	if e != "" {
		body := e + ":" + c.EnvData[e].(string)
		// Store JSON representation of service object
		*env = Env{e, c.EnvData[e]}
		return body, "/env", nil
	}
	return body, ref, nil
}

func (c *AppConfig) submitFormEnvDelete(values url.Values) error {
	var err error = errors.New("submitFormEnvDelete: Ошибка чтения формы")
	e := values.Get("Index")
	if e != "" {
		delete(c.EnvData, e)
		return c.Save(ConfigFile)
	}
	return err
}

func (c *AppConfig) submitFormEnvUpdate(values url.Values, ref string) (string, error) {
	var err error = errors.New("submitFormEnvUpdate: Ошибка чтения формы")

	fmt.Println("submitFormEnvUpdate ", values)

	backRef := ref

	e := values.Get("Key")
	v := values.Get("Value")

	if e != "" && v != "" {
		backRef = "/env"

		if e != "" {
			if c.EnvData == nil {
				c.EnvData = make(map[string]interface{})
			}
			c.EnvData[e] = v
			return backRef, c.Save(ConfigFile)
		}
	}

	return backRef, err

}

// For textarea displaying body
func (c *AppConfig) serviceBody(s int, v int) string {
	body, _ := json.MarshalIndent(c.Servers[s].Services[v], "                    ", "      ")
	rawin := json.RawMessage(body)
	rawbytes, _ := rawin.MarshalJSON()
	r := strings.NewReplacer("\\r\\n", "\r\n",
		"\\u003c", "<",
		"\\u003e", ">")
	return r.Replace(fmt.Sprintf("%s", rawbytes))
}

func (c *AppConfig) prepareServiceToForm(values url.Values, service *Service, ref string) (string, string, error) {
	var body string = ""
	s, errs := strconv.Atoi(values.Get("s"))
	if errs == nil {
		service.serverIndex = s
		v, errv := strconv.Atoi(values.Get("v"))
		if errv == nil {
			c.Servers[s].Services[v].serverIndex = s
			c.Servers[s].Services[v].index = v
			body := c.serviceBody(s, v)
			// Store JSON representation of service object
			*service = c.Servers[s].Services[v]
			return body, "/group?s=" + values.Get("s") +
				"&v=" + values.Get("v"), nil
		}
	} else {
		return body, "/server?s=" + values.Get("s"), errs
	}
	return body, ref, nil
}

func (c *AppConfig) submitFormServiceUpdate(values url.Values, ref string) (string, error) {
	var err error = errors.New("submitFormServiceUpdate: Ошибка чтения формы")

	backRef := ref

	fmt.Println("values: ", values)

	s, errs := strconv.Atoi(values.Get("ServerIndex"))
	v, errv := strconv.Atoi(values.Get("Index"))
	add_to_all := checkBoxState(values.Get("AddToAll"))
	use_json := checkBoxState(values.Get("CreateFromJSON"))

	fmt.Println("s:   ", s, "v: ", v, "add_to_all: ", add_to_all, "use_json: ", use_json)

	if errs == nil && errv == nil {
		backRef = "/group?s=" + values.Get("ServerIndex") + "&v=" + values.Get("Index")

		var defService Service = newDefService()

		if use_json {
			// Create object from JSON
			json.Unmarshal([]byte(values.Get("JSON")), &defService)
			fmt.Println("defService:   ", defService)
		}

		isNewAdded := false
		if v == -1 {
			c.Servers[s].Services = append(c.Servers[s].Services, defService)
			v = len(c.Servers[s].Services) - 1
			isNewAdded = true
			backRef = "/server?s=" + values.Get("ServerIndex")
		}

		if use_json {
			c.Servers[s].Services[v] = defService
			c.Servers[s].Services[v].Enumerate(s, v)
		}

		c.Servers[s].Services[v].Name = values.Get("Name")
		c.Servers[s].Services[v].serverIndex = s
		c.Servers[s].Services[v].index = v

		defService.Name = c.Servers[s].Services[v].Name

		if add_to_all {
			// Create same service in all servers
			for i, _ := range c.Servers {
				if i != s {
					if isNewAdded {
						c.Servers[i].Services = append(c.Servers[i].Services, defService)
					}
					for j, _ := range c.Servers[i].Services {
						if c.Servers[i].Services[j].Name == values.Get("Name") {
							if use_json {
								c.Servers[i].Services[j] = defService
								c.Servers[i].Services[j].Enumerate(i, j)
							}
							c.Servers[i].Services[j].index = j
							c.Servers[i].Services[j].serverIndex = i
						}
					}
				}
			}
		}

		return backRef, c.Save(ConfigFile)
	}

	return backRef, err
}

func (c *AppConfig) submitFormServiceDelete(values url.Values) error {
	var err error = errors.New("submitFormServiceDelete: Ошибка чтения формы")
	s, errs := strconv.Atoi(values.Get("Parent"))
	v, errv := strconv.Atoi(values.Get("Index"))
	if errs == nil && errv == nil {
		err = c.Servers[s].deleteService(v)
		if err == nil {
			return c.Save(ConfigFile)
		}
	}
	return err
}
