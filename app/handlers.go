package app

import (
	"fmt"
	"html/template"
	"net/http"
	"reflect"
	"strconv"
)

type (
	// ConfigBundle - accumulate config and Data with packing algorithm created
	ConfigBundle struct {
		AppConfig AppConfig
		Account   Account
		Object    interface{}
		Base      Base
		Body      string
		Action    string
		Ref       string
	}
)

func currentAccount(r *http.Request) (Account, error) {
	session, err := store.Get(r, "cookie-name")
	if err != nil {
		return Account{Authenticated: false}, err
	}
	return getUser(session), nil
}

func QueryValue(value string, array map[string][]string) (string, error) {
	if len(array) == 0 || len(array[value]) == 0 {
		return "", fmt.Errorf("Failed got value? but not array created.. ")
	}
	return array[value][0], nil
}

// Handlers

func (c *AppConfig) Index() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		dataPage := &ConfigBundle{*c, account, nil, nil, "", "", ""}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/slider.html",
			RootDir + "templates/selector.html",
			RootDir + "templates/testing.html",
			RootDir + "templates/all-servers.html",
		}

		layout := "mainPage"
		if account.Authenticated == true {
			paths = append(paths, RootDir+"templates/main.html")
		} else {
			paths = append(paths, RootDir+"templates/login.html")
			layout = "login"
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err = contactTmpl.ExecuteTemplate(w, layout, dataPage)

		if err != nil {
			panic(err)
		}

	}

}

func (c *AppConfig) Part() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}

		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		defPart := TestPart{Name: "Новая проверка", Script: "ar Result = true; Error = '''; {console.log(Result, ' = ok');}", index: -1, TestIndex: -1}

		values := r.URL.Query()
		t, errt := strconv.Atoi(values.Get("t"))

		if errt == nil {
			defPart.TestIndex = t
			p, errp := strconv.Atoi(values.Get("p"))
			if errp == nil {
				defPart = c.Tests[t].TestParts[p]
				defPart.index = p
			}
		} else {
			r.ParseForm()
			if len(r.Form) != 0 {
				t_str := r.FormValue("TestIndex")
				t, _ := strconv.Atoi(t_str)
				p, _ := strconv.Atoi(r.FormValue("PartIndex"))
				if p == -1 {
					c.Tests[t].TestParts = append(c.Tests[t].TestParts, defPart)
					p = len(c.Tests[t].TestParts) - 1
				}
				c.Tests[t].TestParts[p].Name = r.FormValue("Name")
				c.Tests[t].TestParts[p].Script = r.FormValue("Script")
				c.Tests[t].TestParts[p].index = p
				c.Tests[t].TestParts[p].TestIndex = t
				err := c.Save(ConfigFile)
				if err != nil {
					fmt.Println("Can't save app config, error..")
				}
				http.Redirect(w, r, "/test?t="+t_str, http.StatusFound)
				return
			}
		}

		dataPage := &ConfigBundle{*c, account, defPart, &(defPart), "", "", ""}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/form-part.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "partForm", dataPage)

		if err2 != nil {
			panic(err)
		}

	}
}

func (c *AppConfig) Test() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}

		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		defTest := newDefTest()
		values := r.URL.Query()
		var body, ref string
		if len(values) != 0 {
			body, ref, _ = c.prepareTestToForm(values, &defTest, r.Referer())
		} else {
			r.ParseForm()
			if len(r.Form) != 0 {
				ref, err := c.submitFormTestUpdate(r.Form, r.Referer())
				if err != nil {
					fmt.Println("Can't submit form test for update, error: ", err.Error())
				}
				http.Redirect(w, r, ref, http.StatusFound)
				return
			}
		}

		dataPage := &ConfigBundle{
			*c,
			account,
			defTest,
			&defTest,
			body,
			"/form-test",
			ref}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/form-test.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "testForm", dataPage)

		if err2 != nil {
			panic(err)
		}

	}
}

func (c *AppConfig) Service() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}

		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		defService := newDefService()
		values := r.URL.Query()

		var ref, body string
		if len(values) != 0 {
			body, ref, _ = c.prepareServiceToForm(values, &defService, r.Referer())
		} else {
			r.ParseForm()
			if len(r.Form) != 0 {
				ref, err := c.submitFormServiceUpdate(r.Form, r.Referer())
				fmt.Println("Ref is ", ref)
				if err != nil {
					fmt.Println("Can't submit form service for update, error:", err.Error())
				}
				http.Redirect(w, r, ref, http.StatusFound)
				return
			}
		}

		dataPage := &ConfigBundle{
			*c,
			account,
			defService,
			&defService,
			body,
			"/form-service",
			ref}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/form-service.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "serviceForm", dataPage)

		if err2 != nil {
			panic(err)
		}

	}
}

func (c *AppConfig) Env() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}

		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		defEnv := newDefEnv()
		values := r.URL.Query()

		var ref, body string
		if len(values) != 0 {
			body, ref, _ = c.prepareEnvToForm(values, &defEnv, r.Referer())
		} else {
			r.ParseForm()
			if len(r.Form) != 0 {
				ref, err := c.submitFormEnvUpdate(r.Form, r.Referer())
				fmt.Println("Ref is ", ref)
				if err != nil {
					fmt.Println("Can't submit form env for update, error:", err.Error())
				}
				http.Redirect(w, r, ref, http.StatusFound)
				return
			}
		}

		dataPage := &ConfigBundle{
			*c,
			account,
			defEnv,
			nil,
			body,
			"/form-env",
			ref}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/form-env.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "envForm", dataPage)

		if err2 != nil {
			panic(err)
		}

	}
}

func (c *AppConfig) Parameter() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}

		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		defParameter := newDefParameter()
		values := r.URL.Query()

		var ref string
		if len(values) != 0 {
			ref, _ = c.prepareParamToForm(values, &defParameter, r.Referer())
		} else {
			r.ParseForm()
			if len(r.Form) != 0 {
				ref, err := c.submitFormParamUpdate(r.Form, r.Referer())
				if err != nil {
					fmt.Println("Can't submit parameter for update, error:", err.Error())
				}
				http.Redirect(w, r, ref, http.StatusFound)
				return
			}
		}

		dataPage := &ConfigBundle{
			*c,
			account,
			defParameter,
			&(defParameter),
			"",
			"/form-parameter",
			ref}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/form-parameter.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "parameterForm", dataPage)

		if err2 != nil {
			panic(err)
		}

	}
}

func (c *AppConfig) Server() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}

		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		defServer := Server{Ipaddr: "0.0.0.0:00",
			Cluster: -1,
			Alias:   "Новый ",
			Host:    "",
			index:   -1}

		queryValues := r.URL.Query()
		if len(queryValues) != 0 {
			s, _ := strconv.Atoi(queryValues["s"][0])
			defServer = c.Servers[s]
			defServer.index = s
		} else {
			r.ParseForm()
			if len(r.Form) != 0 {
				index, _ := strconv.Atoi(r.FormValue("Index"))
				if index == -1 {
					c.Servers = append(c.Servers, defServer)
					index = len(c.Servers) - 1
				}
				c.Servers[index].Ipaddr = r.FormValue("Ipaddr")
				c.Servers[index].Alias = r.FormValue("Alias")
				cluster, _ := strconv.Atoi(r.FormValue("Cluster"))
				fmt.Println("Cluster ", cluster)
				c.Servers[index].Cluster = cluster
				c.Servers[index].index = index
				err := c.Save(ConfigFile)
				if err != nil {
					fmt.Println("Can't save app config, error..")
				}
				http.Redirect(w, r, "/", http.StatusFound)
				return
			}
		}

		dataPage := &ConfigBundle{*c,
			account,
			defServer,
			&(defServer),
			"",
			"/form-server",
			"/server?s=" + strconv.Itoa(defServer.index)}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/form-server.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "serverForm", dataPage)

		if err2 != nil {
			panic(err)
		}

	}
}

func (c *AppConfig) ClustersPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}

		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		dataPage := &ConfigBundle{
			*c,
			account,
			nil,
			nil,
			"",
			"",
			""}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/clusters.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "clusters", dataPage)

		if err2 != nil {
			panic(err)
		}

	}
}

type Cluster struct {
	Name  string
	Index int
}

func (c *AppConfig) Cluster() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}

		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		defCluster := Cluster{Name: "Новый кластер", Index: -1}

		queryValues := r.URL.Query()
		if len(queryValues) != 0 {
			сnum, _ := strconv.Atoi(queryValues["c"][0])
			defCluster.Name = c.Clusters[сnum]
			defCluster.Index = сnum
		} else {
			r.ParseForm()
			if len(r.Form) != 0 {
				index, _ := strconv.Atoi(r.FormValue("Index"))
				if index == -1 {
					c.Clusters = append(c.Clusters, defCluster.Name)
					index = len(c.Clusters) - 1
				}
				c.Clusters[index] = r.FormValue("Name")
				err := c.Save(ConfigFile)
				if err != nil {
					fmt.Println("Can't save app config, error..")
				}
				http.Redirect(w, r, "/clusters", http.StatusFound)
				return
			}
		}

		dataPage := &ConfigBundle{
			*c,
			account,
			defCluster,
			nil,
			"",
			"",
			""}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/form-cluster.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "clusterForm", dataPage)

		if err2 != nil {
			panic(err)
		}

	}
}

func (c *AppConfig) Setup() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}
		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		dataPage := &ConfigBundle{
			*c,
			account,
			nil,
			nil,
			"",
			"",
			""}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/setup.html",
			RootDir + "templates/clusters.html",
			RootDir + "templates/env.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "setup", dataPage)

		if err2 != nil {
			panic(err)
		}

	}
}

func (c *AppConfig) TestPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}
		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		// Get params parse
		values := r.URL.Query()
		fmt.Println("Params is ", values)
		defTest := newDefTest()
		body, ref, _ := c.prepareTestToForm(values, &defTest, r.Referer() )

		dataPage := &ConfigBundle{
			*c,
			account,
			defTest,
			&defTest,
			body,
			"",
			ref}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/test.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "testPage", dataPage)

		if err2 != nil {
			panic(err)
		}

	}
}

func (c *AppConfig) EnvPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}
		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		fmt.Println("EnvPage fired..")

		dataPage := &ConfigBundle{
			*c,
			account,
			nil,
			nil,
			"",
			"",
			""}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/env.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "envPage", dataPage)

		if err2 != nil {
			panic(err)
		}

	}
}

func (c *AppConfig) ServicePage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}

		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		// Get params parse
		queryValues := r.URL.Query()
		fmt.Println("Params is ", queryValues)

		s, _ := strconv.Atoi(queryValues["s"][0])
		v, _ := strconv.Atoi(queryValues["v"][0])

		service := c.Servers[s].Services[v]
		service.index = v
		service.serverIndex = s

		dataPage := &ConfigBundle{
			*c,
			account,
			service,
			&service,
			"",
			"",
			""}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/group.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "groupPage", dataPage)

		if err2 != nil {
			panic(err)
		}

	}
}

func (c *AppConfig) ServerPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}
		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		// Get params parse
		values := r.URL.Query()
		fmt.Println("Params is ", values)

		s, errs := strconv.Atoi(values.Get("s"))
		if errs != nil {
			return
		}

		server := c.Servers[s]
		server.index = s
		dataPage := &ConfigBundle{
			*c,
			account,
			server,
			&(server),
			"",
			"",
			""}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/slider.html",
			RootDir + "templates/selector.html",
			RootDir + "templates/server.html",
			RootDir + "templates/single-server.html",
			RootDir + "templates/testing.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "serverPage", dataPage)

		if err2 != nil {
			panic(err)
		}
	}
}

// login authenticates the user
func Login(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Fire login..")
	session, err := store.Get(r, "cookie-name")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		fmt.Println("Error:", err.Error(), ", ", http.StatusInternalServerError)
		return
	}

	r.ParseForm()
	fmt.Println("Receiveed ", r.Form)

	if r.FormValue("username") == "user" && r.FormValue("code") != "code" ||
		r.FormValue("username") == "admin" && r.FormValue("code") != "flvby" {
		if r.FormValue("code") == "" {
			session.AddFlash("Must enter a code")
		}
		session.AddFlash("The code was incorrect")
		err = session.Save(r, w)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/forbidden", http.StatusFound)
		return
	}

	username := r.FormValue("username")

	user := &Account{
		Username:      username,
		Authenticated: true,
		Type:          0,
	}
	if username == "admin" {
		user.Type = 1 // It's admin
	}

	session.Values["user"] = user

	err = session.Save(r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Println("Referer is ", r.Referer())
	http.Redirect(w, r, r.Referer(), http.StatusFound)
}

// logout revokes authentication for a user
func Logout(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, "cookie-name")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	session.Values["user"] = Account{}
	session.Options.MaxAge = -1

	err = session.Save(r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/", http.StatusFound)
}

// secret displays the secret message for authorized users
func Secret(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, "cookie-name")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	user := getUser(session)

	if auth := user.Authenticated; !auth {
		session.AddFlash("You don't have access!")
		err = session.Save(r, w)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		http.Redirect(w, r, "/forbidden", http.StatusFound)
		return
	}

	tpl.ExecuteTemplate(w, "secret.gohtml", user.Username)
}

func Forbidden(w http.ResponseWriter, r *http.Request) {
	// session, err := store.Get(r, "cookie-name")
	// if err != nil {
	// 	http.Error(w, err.Error(), http.StatusInternalServerError)
	// 	return
	// }
	//
	// flashMessages := session.Flashes()
	// err = session.Save(r, w)
	// if err != nil {
	// 	http.Error(w, err.Error(), http.StatusInternalServerError)
	// 	return
	// }
	// tpl.ExecuteTemplate(w, "forbidden.gohtml", flashMessages)
	http.Redirect(w, r, "/", http.StatusFound)
	return
}

func (c *AppConfig) DeleteServer() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}

		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		var i int
		values := r.URL.Query()
		if len(values) != 0 {
			i, _ = strconv.Atoi(values.Get("s"))
			c.Servers[i].index = i
		} else {
			r.ParseForm()
			if len(r.Form) != 0 {
				i, _ := strconv.Atoi(r.FormValue("Index"))
				err := c.deleteServer(i)
				if err != nil {
					fmt.Println(err.Error())
				}
				err = c.Save(ConfigFile)
				if err != nil {
					fmt.Println("Can't save app config, error..")
				}
				http.Redirect(w, r, "/", http.StatusFound)
				return
			}
		}

		dataPage := &ConfigBundle{
			*c,
			account,
			c.Servers[i],
			&(c.Servers[i]),
			"",
			"/delete-server",
			"/server?s=" + strconv.Itoa(i)}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/form-delete.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "deleteForm", dataPage)

		if err2 != nil {
			panic(err)
		}

	}
}

func (c *AppConfig) DeleteTest() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}

		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		queryValues := r.URL.Query()
		var i int
		if len(queryValues) != 0 {
			i, _ = strconv.Atoi(queryValues["t"][0])
			c.Tests[i].index = i
		} else {
			r.ParseForm()
			if len(r.Form) != 0 {
				i, _ := strconv.Atoi(r.FormValue("Index"))
				err := c.deleteTest(i)
				if err != nil {
					fmt.Println(err.Error())
				}
				err = c.Save(ConfigFile)
				if err != nil {
					fmt.Println("Can't save app config, error..")
				}
				http.Redirect(w, r, "/", http.StatusFound)
				return
			}
		}

		dataPage := &ConfigBundle{
			*c,
			account,
			c.Tests[i],
			&(c.Tests[i]),
			"",
			"/delete-test",
			"/"}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/form-delete.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "deleteForm", dataPage)

		if err2 != nil {
			panic(err)
		}

	}
}

func (c *AppConfig) DeletePart() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		values := r.URL.Query()
		var t, p int
		if len(values) != 0 {
			t, _ = strconv.Atoi(values.Get("t"))
			p, _ = strconv.Atoi(values.Get("p"))
			c.Tests[t].TestParts[p].index = p
		} else {
			r.ParseForm()
			if len(r.Form) != 0 {
				t_str := r.FormValue("Parent")
				t, _ = strconv.Atoi(t_str)
				p, _ = strconv.Atoi(r.FormValue("Index"))
				err := c.Tests[t].deletePart(p)
				if err != nil {
					fmt.Println(err.Error())
				}
				err = c.Save(ConfigFile)
				if err != nil {
					fmt.Println("Can't save app config, error..")
				}
				http.Redirect(w, r, "/test?t="+t_str, http.StatusFound)
				return
			}
		}

		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}

		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}
		dataPage := &ConfigBundle{
			*c,
			account,
			c.Tests[t].TestParts[p],
			&(c.Tests[t].TestParts[p]),
			"",
			"/delete-part",
			"/test?t=" + strconv.Itoa(t)}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/form-delete.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "deleteForm", dataPage)

		if err2 != nil {
			panic(err)
		}
	}
}

func (c *AppConfig) DeleteService() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}

		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		defService := newDefService()
		values := r.URL.Query()
		var ref string
		if len(values) != 0 {
			_, ref, _ = c.prepareServiceToForm(values, &defService, r.Referer())
		} else {
			r.ParseForm()
			if len(r.Form) != 0 {
				err := c.submitFormServiceDelete(r.Form)
				if err != nil {
					fmt.Println("Can't delete service, error: ", err.Error())
				}
				http.Redirect(w, r, "/server?s="+r.FormValue("Parent"), http.StatusFound)
				return
			}
		}

		dataPage := &ConfigBundle{
			*c,
			account,
			defService,
			&defService,
			"",
			"/delete-service",
			ref}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/form-delete.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "deleteForm", dataPage)

		if err2 != nil {
			panic(err)
		}
	}
}

func (c *AppConfig) DeleteParameter() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}

		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		var defParameter Parameter
		values := r.URL.Query()

		var ref string
		if len(values) != 0 {
			ref, _ = c.prepareParamToForm(values, &defParameter, r.Referer())
		} else {
			r.ParseForm()
			if len(r.Form) != 0 {
				err = c.submitFormParamDelete(r.Form)
				if err != nil {
					fmt.Println("Can't submit delete parameter, error:", err.Error())
				}
				http.Redirect(w, r,
					"/group?s="+r.FormValue("Root")+
						"&v="+r.FormValue("Parent"),
					http.StatusFound)
				return
			}
		}

		dataPage := &ConfigBundle{
			*c,
			account,
			defParameter,
			&defParameter,
			"",
			"/delete-parameter",
			ref}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/form-delete.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "deleteForm", dataPage)

		if err2 != nil {
			panic(err)
		}
	}
}

func (c *AppConfig) DeleteEnv() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}

		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		var defEnv Env
		values := r.URL.Query()

		var ref, body string
		if len(values) != 0 {
			body, ref, _ = c.prepareEnvToForm(values, &defEnv, r.Referer())
		} else {
			r.ParseForm()
			if len(r.Form) != 0 {
				err = c.submitFormEnvDelete(r.Form)
				if err != nil {
					fmt.Println("Can't submit delete parameter, error:", err.Error())
				}
				http.Redirect(w, r, "/env", http.StatusFound)
				return
			}
		}

		dataPage := &ConfigBundle{
			*c,
			account,
			defEnv,
			&defEnv,
			body,
			"/delete-env",
			ref}

		paths := []string{
			RootDir + "templates/header.html",
			RootDir + "templates/footer.html",
			RootDir + "templates/form-delete.html",
		}

		contactTmpl, _ := template.ParseFiles(paths...)
		err2 := contactTmpl.ExecuteTemplate(w, "deleteForm", dataPage)

		if err2 != nil {
			panic(err)
		}
	}
}

func (c *AppConfig) SaveParameter() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		account, err := currentAccount(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			fmt.Println("Session failed..")
		}

		if account.Authenticated == false {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		r.ParseForm()
		fmt.Println("Parameter name to save: ", r.FormValue("name"), ", value: ", r.FormValue("value"))

		s := reflect.ValueOf(c).Elem()

		if s.Kind() == reflect.Struct {
			if s.IsValid() {
				f := s.FieldByName(r.FormValue("name"))
				fmt.Println("Field ", r.FormValue("name"), " is a ", f)
				if f.CanSet() {
					// change value of name
					fmt.Println("Set value to ", r.FormValue("value"), ", type is ", f.Kind())
					if f.Kind() == reflect.Bool {
						b, _ := strconv.ParseBool(r.FormValue("value"))
						fmt.Println("Is bool , value ", b)
						f.SetBool(b)
					}
					if f.Kind() == reflect.Int {
						b, _ := strconv.ParseInt(r.FormValue("value"), 10, 32)
						fmt.Println("Is Int , value ", b)
						f.SetInt(int64(b))
					}
				}
			}
		}
	}
}
