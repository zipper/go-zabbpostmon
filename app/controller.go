package app

import (
	"encoding/gob"
	"html/template"

	"github.com/gorilla/sessions"
)

type Account struct {
	Username      string `json:"username"`
	Email         string `json:"email"`
	Password      string `json:"password"`
	Token         string `json:"token"`
	Type          uint   `json:"type"`
	Id            uint   `json:"id"`
	Authenticated bool   `json:"authenticated"`
}

// store will hold all session data
var store *sessions.CookieStore

// tpl holds all parsed templates
var tpl *template.Template

func init() {
	authKeyTest := []byte{165, 193, 227, 68, 189, 190, 226, 58, 168, 234, 153, 192, 194, 3, 54, 125, 127, 74, 25, 112, 199, 105, 91, 228, 47, 52, 128, 22, 249, 5, 213, 110, 164, 78, 130, 86, 33, 200, 50, 2, 168, 208, 127, 120, 92, 194, 148, 234, 62, 188, 46, 131, 97, 1, 194, 96, 12, 148, 102, 150, 43, 65, 124, 39}
	authKeyOne := authKeyTest
	// authKeyOne := securecookie.GenerateRandomKey(64)
	encriptionKeyOneTest := []byte{112, 188, 175, 134, 254, 142, 76, 74, 191, 182, 229, 195, 44, 202, 36, 178, 134, 143, 226, 51, 0, 12, 178, 111, 243, 229, 196, 104, 98, 48, 240, 38}
	encryptionKeyOne := encriptionKeyOneTest
	// encryptionKeyOne := securecookie.GenerateRandomKey(32)

	store = sessions.NewCookieStore(
		authKeyOne,
		encryptionKeyOne,
	)

	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   60 * 24 * 60,
		HttpOnly: true,
	}

	gob.Register(Account{})

	// tpl = template.Must(template.ParseGlob("templates/*.html"))
}

/**
func Templates() *template.Template {
	if tpl == nil {
		tpl = template.Must(template.New("").Funcs(template.FuncMap{
			"args": func(vs ...interface{}) []interface{} { return vs },
		}).ParseGlob(RootDir + "templates/*.html"))
	}
	fmt.Println(RootDir + "templates/*.html")
	return tpl
}*/

// getUser returns a user from session s
// on error returns an empty user
func getUser(s *sessions.Session) Account {
	val := s.Values["user"]
	var user = Account{}
	user, ok := val.(Account)
	if !ok {
		return Account{Authenticated: false}
	}
	return user
}
