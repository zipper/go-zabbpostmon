package app

func newDefEnv() Env {
	return Env{"", ""}
}

func (e *Env) PublicName() string {
	return e.Key
}

func (e *Env) Index()  interface{} {
	return  e.Key
}

func (e *Env) Parent() int {
	return -1
}
func (e *Env) Root() int  {
	return -1
}

func (e *Env) Error() bool {
	return false
}

func (e *Env) Status() string {
	return "Успешно"
}