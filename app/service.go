package app

import (
	"fmt"
)

func newDefService() Service {
	return Service{Name: "Новый сервис", index: -1, serverIndex: -1}
}

func (g *Service) StatusTypeCount(status int) int {
	counter := 0
	for _, p := range g.Parameters {
		if status == int(p.TriggerStatus) {
			counter++
		}
	}
	return counter
}

func (g *Service) Toggled() int32 {
	find := func(g *Service) int32 {
		var c int32 = 0
		for _, p := range g.Parameters {
			if p.TriggerStatus > 0 {
				c++
			}
		}
		return c
	}

	return find(g)
}

func (g *Service) deleteParameter(index int) error {
	if index < 0 {
		return fmt.Errorf("Index low than range ", index)
	}
	CollectoMutex.Lock()
	g.Parameters[len(g.Parameters)-1], g.Parameters[index] = g.Parameters[index], g.Parameters[len(g.Parameters)-1]
	g.Parameters = g.Parameters[:len(g.Parameters)-1]
	CollectoMutex.Unlock()
	return nil
}

// Need after block copy but source not have indexes
func (g *Service) Enumerate(s int, v int) {
	for i, p := range g.Parameters {
		p.Pid = i;
		p.ServiceIndex = v
		p.ServerIndex = s
	}
}

func (g *Service) PublicName() string {
	return g.Name
}

func (g *Service) Index() interface{} {
	return g.index
}

func (g *Service) Parent() int {
	return g.serverIndex
}

func (g *Service) Root() int {
	return -1
}

func (g *Service) Error() bool {
	return false
}

func (g *Service) Status() string {
	return ""
}
