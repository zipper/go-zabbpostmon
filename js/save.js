// +function ($) {
//     var save = function () {
//             $(".save").click(function (e) {
//                 console.log($(e.currentTarget).data("name")
//                     ,", ", $(e.currentTarget).data("value"));
//                 // $(".loadingDiv").show();
//                 $.ajax({
//                     url: '/save-parameter',
//                     type: 'post',
//                     dataType: 'html',
//                     data: {name: $(e.currentTarget).data("name"),
//                         value: $(e.currentTarget).data("value")
//                     },
//                 });
//                 return true;
//             });
//         },
//         initOnLoad = function () {
//             // Catch Worker
//             window.addEventListener('load', save, false);
//         };
//
//     $(document).ready(function () {
//         initOnLoad();
//     });
//
// }(jQuery);

function saveParameter(name, value) {
    console.log(name,", ", value);
    $.ajax({
        url: '/save-parameter',
        type: 'post',
        dataType: 'html',
        data: { name: name, value: value },
    });
}