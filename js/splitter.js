(function ($) {

    // We should take in an argument for endDrag
    $.fn.VerticalSplit = function () {
        console.log("Fire vertical split on", $(this).selector);
        var selector = $(this).selector;
        var maxHeight = 0;
        var carrier = null;
        var capture = false;
        var xPos = 0, prevxPos = 0, firstOffset = 0, lastOffset = 0, lastWidth = 0;
        var slaves = [];

        $().ready($.proxy(function () {
            var screenWidth = function (object) {
                    if (object == void 0) {
                        return 0;
                    }
                    return object.clientWidth
                        + parseInt($(object).css('padding-right'))
                        + parseInt($(object).css('padding-left'))
                        + parseInt($(object).css('margin-right'))
                        + parseInt($(object).css('margin-left'));
                },
                width = function (object) {
                    if (object == void 0) {
                        return 0;
                    }
                    return object.clientWidth
                        - parseInt($(object).css('padding-right'))
                        - parseInt($(object).css('padding-left'))
                        - parseInt($(object).css('margin-right'))
                        - parseInt($(object).css('margin-left'));
                };
            // Calc the nax children height and select carrier of splitter window
            console.log("Have ", $(selector)[0].childElementCount, " children in ", selector, maxHeight);
            $(selector).children().each(function (i, o) {
                var center = parseInt(($(o).offset().left + screenWidth(o)) / 2);
                slaves[center] = o;
                console.log("Object height is ", $(o).height(), ", offset.x = ", $(o).offset().left, ", width", screenWidth(o));
                if ($(o).height() > maxHeight) {
                    maxHeight = $(o).height();
                    carrier = o;
                }
            });

            if (carrier != null) {
                // Ctreate splitter window
                var splitter = $("<div></div>").prependTo(carrier).css({
                    width: "4px",
                    height: maxHeight,
                    backgroundColor: "#transparent",
                    position: "absolute",
                    left: "-4px",
                    borderLeft: "1px solid rgba(110, 191, 110, 1)",
                    borderRight: "1px solid rgba(110, 191, 110, 1)",
                    opacity: "0.2"
                }).attr("id", "splitter-" + selector);

                $(splitter).hover(function () {
                    $(this).css("cursor", "col-resize")
                });

                $(splitter).mousedown($.proxy(function () {
                    // Lock splitter
                    capture = true;
                    firstOffset = $(carrier).offset().left;
                    lastWidth = width(carrier) + firstOffset;
                    prevxPos = 0;
                    console.log("xPos = ", xPos, ", lastWidth = ", lastWidth);
                    return false;
                }, this));

                $($(splitter).attr("id") + ", " + selector).mouseup($.proxy(function () {
                    // UnLock splitter
                    capture = false;
                }, this));

                $($(splitter).attr("id") + ", " + selector).mouseleave($.proxy(function () {
                    // UnLock splitter
                    lastOffset = 0;
                    capture = false;
                }, this));

                $(selector).mousedown($.proxy(function () {
                    // UnLock splitter
                    lastOffset = 0;
                    capture = false;
                }, this));

                $(selector).mousemove($.proxy(function (e) {
                    if (!capture) {
                        return true;
                    }
                    console.log("e.pageX = ", e.pageX, " capture = ", capture);
                    // UnLock splitter
                    xPos = e.pageX;
                    lastOffset += (xPos - prevxPos);
                    lastWidth -= (xPos - prevxPos);

                    $(carrier).offset({left: lastOffset});
                    $(carrier).width(lastWidth);

                    prevxPos = xPos;

                    console.log("offset ", $(carrier).offset(), ", [ xPos - prevxPos ] "
                        , xPos - prevxPos, ", lastWidth = ", lastWidth, ", firstOffset = ", firstOffset);

                }, this));

                console.log(splitter);
            }


        }, this));

    }
}(jQuery));