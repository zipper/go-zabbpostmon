+function ($) {
    var
        /**
         * Create pseudo multithreaded process with
         * working quasi nodes
         * @returns {undefined}
         */
        createMonitor = function () {
            // Handle toggle loading.gif for real link for blocking download
            $("a").unbind().click(function (e) {
                if ($(e.target).attr("href") == "#" ||
                    $(e.currentTarget).attr("href") == "#") {
                    e.preventDefault();
                } else {
                    $(`<div class="loadingDiv">
                    <div>
                        <h7>Загрузка...</h7>
                    </div>
                    </div>`).prependTo(document.body).show();
                }
            });

            // Handle click on server link for show page server
            $("a.server-link").unbind().click(function (e) {
                console.log($(e.currentTarget).data("server"));
                $(".loadingDiv").show();
                $.ajax({
                    url: '/server',
                    type: 'post',
                    dataType: 'html',
                    data: {number: $(e.currentTarget).data("server")},
                    success: function (data) {
                        $(".loadingDiv").hide();
                        $('.right-wrapper').html(data);
                    },
                });
                return false;
            });

        },

        /**
         * Entry point of sripts
         * @returns {undefined}
         */
        initOnLoad = function () {
            // Catch Worker
            window.addEventListener('load', createMonitor, false);
        };

    $(document).ready(function () {
        initOnLoad();
    });

}(jQuery);