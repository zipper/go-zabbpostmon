$(document).ready(function () {
    // Handle click on server page style bar
    $("ul.page-style > li > a").unbind().click(function (e) {
        // console.log($(e.target).data("style"));
        $.ajax({
            url: "/server",
            type: "post",
            dataType: "html",
            data: {
                number: $(e.target).data("server"),
                page: $(e.target).data("style")
            },
            success: function (data) {
                $(".server-page").html(data);
                $(".page-room").html(document.pageName)
            },
            error: function () {
                $(".page-room").html("Ошибка загрузки..")
            }
        });
        return false;
    });

    /*
    // Handle click on group link for show page server
    $("a.group-link").unbind().click(function (e) {
        console.log("Server ", $(e.currentTarget).data("server"), ", Group ", $(e.currentTarget).data("group"));
        $(".loadingDiv").show();
        $.ajax({
            url: '/server',
            type: 'post',
            dataType: 'html',
            data: {
                number: $(e.currentTarget).data("server"),
                group: $(e.currentTarget).data("group")
            },
            success: function (data) {
                $(".loadingDiv").hide();
                $('.main-page').html(data);
            },
        });
        return false;
    });
     */

});