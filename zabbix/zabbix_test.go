package zabbix

import (
	"fmt"
	"testing"
)

func TestDataLen(t *testing.T) {
	data := []byte("ZBXD\x0112345678")
	dataLen := PackDataLen(data)
	fmt.Printf("Packed len of [%x] is [%x], test len is [%d]\n", dataLen, data, len(data))
	length := UnpackDataLen(dataLen)
	fmt.Printf("Unpacked len is [%d]\n", length)
}
