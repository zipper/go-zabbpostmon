// Package implement zabbix sender protocol for send metrics to zabbix.
package zabbix

import (
	"encoding/json"
	"fmt"
	"io"
	"net"

	"evomon-server/app"
)

// Handles incoming requests.
func HandleRequest(conn net.Conn, config *app.AppConfig) {
	// Make a buffer to hold incoming data.
	// big buffer
	var buff []byte

	// Data packet length
	var length uint32

	// using small tmp buffer for demonstrating
	tmp := make([]byte, 1024)

	// Read the incoming connection into the buffer.
	var reads int = 0
	for {
		m, err := conn.Read(tmp)
		if err != nil {
			if err != io.EOF {
				fmt.Println("Error reading: ", err)
			}
			break
		}

		if len(buff) == 0 {
			if m > 13 {
				length = app.UnpackDataLen(tmp[5:13])
				// fmt.Printf("Decoded data packet size [ %d  ] \n", length)
				buff = make([]byte, 0, length)
				buff = append(buff, tmp[13:m]...)
			}
		} else {
			buff = append(buff, tmp[:m]...)
		}

		reads += m

		if reads >= (int(length) + 13) {
			break
		}

	}

	// fmt.Printf("Reads [ %s  ], len %d \n", string(buff), reads)
	// Close the connection when you're done with it.
	conn.Close()

	// Parse json to equal host Group
	var entries app.Reads

	// unmarshal the data into the slice
	if err := json.Unmarshal(buff, &entries); err != nil {
		// fmt.Println("Ошибка разбора пакета...")
		return
	}

	// fmt.Printf("Parse data success [ %v ] \n", entries)

	// Save received data to equal groups
	config.AssignActiveReads(&entries)

}
