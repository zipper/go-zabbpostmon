# go-zabbpostmon

#### Backend server for:

1. receiving data from Zabbix agents and transmitting via HTTP (S) current statistics on the observed data to the frontend;
2. Launch REST api tests to verify the operability of the services of downloading and updating mobile applications;
3. Application management on hosts observed by zabbix agents; 

---
#### Build

1. Install last go for you platform (google will help). Need go module;
2. Clone code:

```
git clone git@bitbucket.org:zipper/go-zabbpostmon.git
```
3. cd to root clones directory and run
```
go build
```
4. Edit defaults port in ```evomon-server.json``` if that needed:
```
"http_port": 8808,
"active_port": 10052
```
5. Installs and setup zabbix agent to yours server(s), see google
6. The zabbix config should be contain IP addrress and port zabbix server - setup our data from ```evomon-server.json``` to that fields;
7. Run zabbix-agents;
8. Run evomon-server;
9. Execute link in browser to evomon-server address;

![alt text](img/04.05.2020.png "Look")

[Some info (by russians)](doc/main.md)

