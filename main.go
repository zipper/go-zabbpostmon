package main

import (
	"context"
	"flag"
	`fmt`
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"

	"github.com/gorilla/mux"

	. "evomon-server/app"
	. "evomon-server/zabbix"
)

func main() {
	var wait time.Duration

	// Init flags and constants usage
	flag.StringVar(&ConfigFile, "config", "./evomon-server.json", "the full path to config *.json file")
	flag.StringVar(&RootDir, "root", "./", "the full path to root dir of app")
	flag.DurationVar(&wait, "graceful-timeout", time.Second*15, "the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m")

	flag.Parse()

	// we initialize our Servers data array
	var config AppConfig
	config.Init()

	err := config.Load(ConfigFile)

	if err != nil {
		panic("Can't load app config, fatal error..")
	}

	router := mux.NewRouter()

	router.HandleFunc("/", config.Index()).Methods("GET")
	router.HandleFunc("/group", config.ServicePage()).Methods("GET")
	router.HandleFunc("/test", config.TestPage()).Methods("GET")
	router.HandleFunc("/server", config.ServerPage()).Methods("GET")
	router.HandleFunc("/setup", config.Setup()).Methods("GET")
	router.HandleFunc("/clusters", config.ClustersPage()).Methods("GET")
	router.HandleFunc("/env", config.EnvPage()).Methods("GET")

	router.HandleFunc("/delete-server", config.DeleteServer())
	router.HandleFunc("/delete-part", config.DeletePart())
	router.HandleFunc("/delete-test", config.DeleteTest())
	router.HandleFunc("/delete-service", config.DeleteService())
	router.HandleFunc("/delete-parameter", config.DeleteParameter())
	router.HandleFunc("/delete-env", config.DeleteEnv())

	router.HandleFunc("/form-test", config.Test())
	router.HandleFunc("/form-part", config.Part())
	router.HandleFunc("/form-service", config.Service())
	router.HandleFunc("/form-env", config.Env())
	router.HandleFunc("/form-cluster", config.Cluster())
	router.HandleFunc("/form-server", config.Server())
	router.HandleFunc("/form-parameter", config.Parameter())

	router.HandleFunc("/login", Login).Methods("POST")
	router.HandleFunc("/save-parameter", config.SaveParameter()).Methods("POST")
	router.HandleFunc("/logout", Logout).Methods("GET")
	router.HandleFunc("/forbidden", Forbidden)

	router.PathPrefix("/").Handler(http.FileServer(http.Dir(RootDir)))

	http.Handle("/", router)

	srv := &http.Server{
		Addr: "0.0.0.0:" + strconv.Itoa(int(config.HttpPort)),
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		// Pass our instance of gorilla/mux in.
		Handler: router,
	}

	log.Println("HttpServer starts on 0.0.0.0:" + strconv.Itoa(int(config.HttpPort)))
	// Run our server in a goroutine so that it doesn't block.
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Fatal("ListenAndServe: ", err)
		}
	}()

	// Listen for incoming connections.
	listener, errList := net.Listen("tcp", "0.0.0.0:"+strconv.Itoa(int(config.ActivePort)))
	if errList != nil {
		fmt.Println("Error listening:", errList.Error())
		os.Exit(1)
	}

	log.Println("Listening on 0.0.0.0:" + strconv.Itoa(int(config.ActivePort)))

	// Listen incoming connection and send responce in routine
	go func() {
		for {
			// Listen for an incoming connection.
			conn, err := listener.Accept()
			if err != nil {
				log.Fatal("TCP Server Accept:", err)
			}
			// Handle connections in a new goroutine.
			go HandleRequest(conn, &config)
		}
	}()

	// Collate periodocally data for backend store
	go func() {

		// The first collate
		config.Collate()

		ticker := time.NewTicker(time.Duration(config.Tickdelay) * time.Second)
		defer func() {
			ticker.Stop()
		}()

		for {
			select {
			case <-ticker.C:
				{
					config.Collate()
					fmt.Println("Step loading success, delay is ", config.Tickdelay)
				}
			}
		}
	}()

	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	srv.Shutdown(ctx)

	listener.Close()
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	log.Println("shutting down")
	os.Exit(0)
}
