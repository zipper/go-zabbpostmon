package db

import (
	//	"database/sql"
	//	"fmt"
	//	"strconv"
	"testing"

	//	_ "github.com/mattn/go-sqlite3"
)

func TestDatabase(t *testing.T) {
	db := DbConn{driver: "sqlite3", dbname: "./test.db"}

	// Open database
	err := db.Open()
	if err != nil {
		t.Error("Error is ", err.Error())
	} else {
		t.Logf("Database %v created successfully", db.dbname)
	}

	// Create table
	q := `CREATE TABLE IF NOT EXISTS userinfo (
    			contact_id INTEGER PRIMARY KEY,
			username TEXT NOT NULL,
    			departname TEXT NOT NULL,
			created TEXT NOT NULL,
    			email TEXT NOT NULL,
    			phone TEXT NOT NULL)`

	err = db.ExecQuery(q)

	if err != nil {
		t.Error("Error is ", err.Error())
	} else {
		t.Log("Create table userinfo sacceded")
	}

	err = db.ExecQuery("INSERT INTO userinfo(username, departname, created, email, phone) values(?,?,?,?,?)", "astaxie", "研发部门", "2012-12-09", "arasras@gmail.com", "+7910648551821")

	if err != nil {
		t.Error("Error is ", err.Error())
		return
	} else {
		t.Log("Inset data to userinfo sacceded")
	}

	id, _ := db.LastInsertId()

	t.Logf("Last id is %d", id)

	data, err1 := db.Query("SELECT * FROM userinfo")

	if err1 != nil {
		t.Error("Error is ", err1.Error())
	} else {
		for m := range data {
			t.Log(data[m])
		}
	}

}

