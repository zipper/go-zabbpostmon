package db

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"

	"evomon-server/app"
)


// DbConn Database management struct, singlethreaded
type DbConn struct {
	driver string
	dbname string
	db     *sql.DB
	stmt   *sql.Stmt
	res    sql.Result
	rows   *sql.Rows
	err    error
}

// DataBase connect descriptor
var DataBase DbConn

func init() {
	DataBase := DbConn{driver: "sqlite3", dbname: app.RootDir + "evomon.db"}

	// Open database
	err := DataBase.Open()
	if err != nil {
		fmt.Println("Error is ", err.Error())
	} else {
		fmt.Println("Database %v created successfully", DataBase.dbname)
	}

}

func GetDB() *DbConn {
	return &DataBase
}

// Open Open database connection
func (o *DbConn) Open() error {
	o.db, o.err = sql.Open(o.driver, o.dbname)
	return o.err
}

// Execute query with prrepared context with args
func (o *DbConn) ExecQuery(query string, args ...interface{}) error {
	o.stmt, o.err = o.db.Prepare(query)
	if o.err != nil {
		return o.err
	}
	if o.stmt == nil {
		return fmt.Errorf("DbConn.ExecQuery: statement is nil pointer")
	}
	o.res, o.err = o.stmt.Exec(args...)
	return o.err
}

// Execute query directed without explicit statement creation
func (o *DbConn) Exec(query string, args ...interface{}) (sql.Result, error) {
	return o.db.Exec(query, args...)
}

// LastId Got last id in updated table after insert
func (o *DbConn) LastInsertId(args ...interface{}) (int64, error) {
	return o.res.LastInsertId()
}

// RowsAffected Get count rows that workerly updated
func (o *DbConn) RowsAffected() (int64, error) {
	return o.res.RowsAffected()
}

// Query Get all quering data in map container
func (o *DbConn) Query(query string, args ...interface{}) (map[int64]map[string]interface{}, error) {
	o.rows, o.err = o.db.Query(query, args...)

	if o.err != nil {
		return nil, o.err
	}

	defer o.rows.Close()

	cols, _ := o.rows.Columns()

	// Output map
	outMap := make(map[int64]map[string]interface{})

	var v int64

	for v = 0; o.rows.Next(); v++ {
		// Create a slice of interface{}'s to represent each column,
		// and a second slice to contain pointers to each item in the columns slice.
		columns := make([]interface{}, len(cols))
		columnPointers := make([]interface{}, len(cols))
		for i := range columns {
			columnPointers[i] = &columns[i]
		}

		// Scan the result into the column pointers...
		if o.err = o.rows.Scan(columnPointers...); o.err != nil {
			return nil, o.err
		}

		// Create our map, and retrieve the value for each column from the pointers slice,
		// storing it in the map with the name of the column as the key.
		m := make(map[string]interface{})

		for i, colName := range cols {
			val := columnPointers[i].(*interface{})
			m[colName] = *val
		}

		// Outputs: map[columnName:value columnName2:value2 columnName3:value3 ...]
		// fmt.Println(m)

		outMap[v] = m
	}

	return outMap, nil
}

// Delete delete row by specific args
func (o *DbConn) Delete(query string, args ...interface{}) (int64, error) {
	o.stmt, o.err = o.db.Prepare(query)

	if o.err != nil {
		return 0, o.err
	}

	o.res, o.err = o.stmt.Exec(args)
	if o.err != nil {
		return 0, o.err
	}

	return o.RowsAffected()
}

